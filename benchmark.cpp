#include <stream9/urls/parser.hpp>

#include <string_view>

#include <boost/timer/timer.hpp>

namespace urls { using namespace stream9::urls; }

int main()
{
    using namespace std::literals;

    auto const input =
        //"http://foo@www.google.com:8080/path?foo=bar#segment"sv;
        //"http://foo@www.google.com:8080/path?foofoofoo=barbarbar#segment"sv;
        "http://www.google.com:8080/path?foo=bar#segment"sv;

    boost::timer::auto_cpu_timer t;

    urls::components result;

    for (auto i = 0; i < 1000000; ++i) {
        result = urls::parse(input);
    }
}
