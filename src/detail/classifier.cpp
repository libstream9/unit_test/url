#include "src/detail/validator.hpp"

#include "../utility.hpp"

#include <string>
#include <string_view>
#include <iomanip>

#include <boost/test/unit_test.hpp>
#include <boost/format.hpp>

namespace testing {

using namespace std::literals;
namespace validator = stream9::urls::validator;

template<typename Fn>
void test_success(std::string_view const s, Fn func)
{
    for (auto const c: s) {
        BOOST_CHECK_MESSAGE(
            func(c),
            "incorrectly failed: "
                << boost::format("0x%02X (%c)")
                    % (int)c % c
        );
    }
}

template<typename Fn>
void test_failure(std::string_view const s, Fn func)
{
    for (char const c: s) {
        BOOST_CHECK_MESSAGE(
            !func(c),
            "incorrectly failed: "
                << boost::format("0x%02X (%c)")
                    % (int)c % c
        );
    }
}

template<typename Fn>
void test(std::string_view const s, Fn func)
{
    test_success(s, func);
    test_failure(invert(s), func);
}

BOOST_AUTO_TEST_SUITE(detail_)

BOOST_AUTO_TEST_SUITE(classifier_)

    BOOST_AUTO_TEST_CASE(is_alpha_)
    {
        auto const s = "abcdefghijklmnopqrstuvwxyz"
                       "ABCDEFGHIJKLMNOPQRSTUVWXYZ"sv;

        test(s, validator::is_alpha);
    }

    BOOST_AUTO_TEST_CASE(is_digit_)
    {
        auto const s = "0123456789"sv;

        test(s, validator::is_digit);
    }

    BOOST_AUTO_TEST_CASE(is_hexdig_)
    {
        auto const s = "0123456789ABCDEFabcdef"sv;

        test(s, validator::is_hexdig);
    }

    BOOST_AUTO_TEST_CASE(is_unreserved_)
    {
        auto const s = "abcdefghijklmnopqrstuvwxyz"
                       "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                       "0123456789"
                       "-._~"sv;

        test(s, validator::is_unreserved);
    }

    BOOST_AUTO_TEST_CASE(is_sub_delims_)
    {
        auto const s = "!$&'()*+,;=";

        test(s, validator::is_sub_delims);
    }

BOOST_AUTO_TEST_SUITE_END() // classifier_

BOOST_AUTO_TEST_SUITE_END() // detail_

} // namespace testing

