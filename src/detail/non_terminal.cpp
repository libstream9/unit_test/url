#include "src/detail/validator.hpp"

#include "../utility.hpp"

#include <string_view>
#include <vector>

#include <boost/format.hpp>
#include <boost/test/unit_test.hpp>

using namespace std::literals;

namespace testing {

namespace validator = stream9::urls::validator;

template<typename Parser>
void success(Parser parser, std::string_view const s)
{
    auto [it, end] = begin_end(s);
    auto err = validator::null_error_handler;

    while (it != end) {
        BOOST_CHECK_MESSAGE(
            parser(it, end, err),
            boost::format("incorrectly failed: %c (0x%02x)")
                % *it % (int)(unsigned char)*it
        );
    }
    BOOST_TEST(it == end);
}

template<typename Parser>
void fail(Parser parser, std::string_view const s)
{
    auto [it, end] = begin_end(s);
    auto err = validator::null_error_handler;

    for (; it != end; ++it) {
        auto before = it;

        BOOST_CHECK_MESSAGE(
            !parser(it, end, err),
            boost::format("incorrectly succeded: %c (0x%02x)")
                % *it % (int)(unsigned char)*it
        );

        if (it != before) {
            BOOST_CHECK_MESSAGE(false,
                "input has incorrectly consumed"
            );
            it = before;
        }
    }
}

template<typename Parser>
void
success(Parser parser, std::vector<std::string_view> const& strings)
{
    auto err = validator::null_error_handler;

    for (auto const& s: strings) {
        auto [it, end] = begin_end(s);
        BOOST_CHECK_MESSAGE(
            parser(it, end, err),
            boost::format("incorrectly failed at %d: %s")
                % (it - s.begin()) % s
        );
        BOOST_CHECK_MESSAGE(
            it == end,
            boost::format("post condition failure: %s") % s
        );
    }
}

template<typename Parser>
void
partial(Parser parser, std::vector<std::string_view> const& strings)
{
    auto err = validator::null_error_handler;

    for (auto const& s: strings) {
        auto [it, end] = begin_end(s);
        BOOST_CHECK_MESSAGE(
            parser(it, end, err),
            boost::format("incorrectly failed: %s") % s
        );
        BOOST_CHECK_MESSAGE(
            it != end,
            boost::format("incorrectly succeded: %s") % s
        );
        BOOST_CHECK_MESSAGE(
            it < end,
            boost::format("post condition failure: %s") % s
        );
    }
}

template<typename Parser>
void
fail(Parser parser, std::vector<std::string_view> const& strings)
{
    auto err = validator::null_error_handler;

    for (auto const& s: strings) {
        auto [it, end] = begin_end(s);
        auto const before = it;
        BOOST_CHECK_MESSAGE(
            !parser(it, end, err),
            boost::format("incorrectly succeeded: %s") % s
        );
        BOOST_CHECK_MESSAGE(
            it == before,
            boost::format("post condition failure at (+%d): %s")
                % (it - s.begin()) % s
        );
    }
}

BOOST_AUTO_TEST_SUITE(validator_)

BOOST_AUTO_TEST_SUITE(non_terminal_)

    BOOST_AUTO_TEST_CASE(unreserved_)
    {
        auto const s = "abcdefghijklmnopqrstuvwxyz"
                       "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                       "0123456789"
                       "-._~";

        success(validator::unreserved, s);
        fail(validator::unreserved, invert(s));
    }

    BOOST_AUTO_TEST_CASE(hexdig_)
    {
        auto const s = "0123456789abcdefABCDEF";

        success(validator::hexdig, s);
        fail(validator::hexdig, invert(s));
    }

    BOOST_AUTO_TEST_CASE(sub_delims_)
    {
        auto const s = "!$&'()*+,;=";

        success(validator::sub_delims, s);
        fail(validator::sub_delims, invert(s));
    }

    BOOST_AUTO_TEST_CASE(pct_encoded_)
    {
        success(validator::pct_encoded, {
            "%01", "%02", "%ab", "%CD", "%FF"
        });

        fail(validator::pct_encoded, {
             "01", "%MN", "$AB", "%AG"
        });
    }

    BOOST_AUTO_TEST_CASE(dec_octet_success)
    {
        success(validator::dec_octet, {
            "0", "9",
            "10", "19",
            "20", "29",
            "30", "39",
            "40", "49",
            "50", "59",
            "60", "69",
            "70", "79",
            "80", "89",
            "90", "99",
            "100", "199",
            "200", "249",
            "250", "255"
        });
    }

    BOOST_AUTO_TEST_CASE(dec_octet_partial)
    {
        partial(validator::dec_octet, {
             "00", "5b", "1F", "1.3"
        });
    }

    BOOST_AUTO_TEST_CASE(dec_octet_fail)
    {
        fail(validator::dec_octet, {
             "A0", "!3", "ZZ",
        });
    }

    BOOST_AUTO_TEST_CASE(pchar_success)
    {
        auto const s =
            // unreserved
            "abcdefghijklmnopqrstuvwxyz"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "0123456789"
            "-._~"
            // sub-delims
            "!$&'()*+,;="
            // etc
            ":@";

        success(validator::pchar, s);

        // include pct-encoded
        success(validator::pchar, "foo%2fbar");
    }

    BOOST_AUTO_TEST_CASE(pchar_fail)
    {
        auto const s =
            // unreserved
            "abcdefghijklmnopqrstuvwxyz"
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
            "0123456789"
            "-._~"
            // sub-delims
            "!$&'()*+,;="
            // etc
            ":@";

        fail(validator::pchar, invert(s));
    }

    BOOST_AUTO_TEST_CASE(segment_success)
    {
        success(validator::segment, {
            "foo", "bar", "C:", ""
        });
    }

    BOOST_AUTO_TEST_CASE(segment_partial)
    {
        partial(validator::segment, {
            "foo/bar", "A|"
        });
    }

    BOOST_AUTO_TEST_CASE(segment_nz_success)
    {
        success(validator::segment_nz, {
            "foo", "bar", "C:",
        });
    }

    BOOST_AUTO_TEST_CASE(segment_nz_fail)
    {
        fail(validator::segment_nz, std::vector<std::string_view> {
            "", "",
        });
    }

    BOOST_AUTO_TEST_CASE(ipv4_address_success)
    {
        success(validator::ipv4_address, {
            "127.0.0.1"sv,
            "192.168.1.1"sv,
        });
    }

    BOOST_AUTO_TEST_CASE(ipv4_address_partial)
    {
        partial(validator::ipv4_address, {
            "192.168.1.350",
            "192.168.1.0D",
        });
    }

    BOOST_AUTO_TEST_CASE(ipv4_address_fail)
    {
        fail(validator::ipv4_address, {
            "127.-15.0.1"sv,
            "127.0.256.1"sv,
        });
    }

    BOOST_AUTO_TEST_CASE(reg_name_success)
    {
        success(validator::reg_name, {
            "foo",
            "foo%3fbar",
            "www.google.com",
            ""
        });
    }

    BOOST_AUTO_TEST_CASE(reg_name_partial)
    {
        partial(validator::reg_name, {
            "foo|",
            "foo%%3fbar",
            "www.google<com",
        });
    }

    BOOST_AUTO_TEST_CASE(ipvfuture_success)
    {
        success(validator::ipvfuture, {
            "v8.foobar",
            "v7d.foo:bar",
            "v7d.a",
        });
    }

    BOOST_AUTO_TEST_CASE(ipvfuture_partial)
    {
        partial(validator::ipvfuture, {
            "v8.foo|bar",
            "v7d.foo@bar",
            "v7d.a|",
        });
    }

    BOOST_AUTO_TEST_CASE(ipvfuture_fail)
    {
        fail(validator::ipvfuture, {
            "v8*.foo|bar",
            "x7d.foo@bar",
            "v7d-foo@bar",
            "v9."
        });
    }

    BOOST_AUTO_TEST_CASE(h16_success)
    {
        success(validator::h16, {
            "1", "12", "123", "123F",
        });
    }

    BOOST_AUTO_TEST_CASE(h16_partial)
    {
        partial(validator::h16, {
            "12345", "ABCDEF",
            "123X", "DEFG",
        });
    }

    BOOST_AUTO_TEST_CASE(h16_fail)
    {
        fail(validator::h16, {
            "x36"sv, ""sv,
        });
    }

    BOOST_AUTO_TEST_CASE(ls32_success)
    {
        success(validator::ls32, {
            "1:2", "ABCD:1234",
            "192.168.1.1", "127.0.0.1",
        });
    }

    BOOST_AUTO_TEST_SUITE(ipv6_address_0)

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                ""sv, ""sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_0

    BOOST_AUTO_TEST_SUITE(ipv6_address_1)
        // 6( h16 ":" ) ls32
        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2:3:4:5:6:7:8"sv,
                "1:2:3:4:5:6:192.168.1.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1:2:3:4:5:6:7:8]",
                "1:2:3:4:5:6:192.168.1.1d",
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1:2:3:4:5:6:7:"sv,
                "1:2:3:4:5:6:192.168.1"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_1

    BOOST_AUTO_TEST_SUITE(ipv6_address_2)
        // "::" 5( h16 ":" ) ls32
        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "::1:2:3:4:5:6:7"sv,
                "::1:2:3:4:5:127.0.0.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "::1:2:3:4:5:6:7]"sv,
                "::1:2:3:4:5:127.0.0.1d"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                ":;1:2:3:4:5:6:7"sv,
                ";:X:2:3:4:5:6:7"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_2

    BOOST_AUTO_TEST_SUITE(ipv6_address_3)
        // [ h16 ] "::" 4( h16 ":" ) ls32
        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1::2:3:4:5:6:7"sv,
                "1::2:3:4:5:127.0.0.1"sv,
                "::1:2:3:4:5:6"sv,
                "::1:2:3:4:127.0.0.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1::2:3:4:5:6:7]"sv,
                "1::2:3:4:5:127.0.0.1d"sv,
                "::1:2:3:4:5:6:"sv,
                "::1:2:3:4:5:127.0.0.l"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1:;2:3:4:5:6:"sv,
                "X::2:3:4:5:6"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_3

    BOOST_AUTO_TEST_SUITE(ipv6_address_4)
        // [ *1( h16 ":" ) h16 ] "::" 3( h16 ":" ) ls32
        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2::3:4:5:6:7"sv,
                "1::2:3:4:5:6"sv,
                "::1:2:3:4:5"sv,
                "1:2::3:4:5:127.0.0.1"sv,
                "1::2:3:4:127.0.0.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1:2::3:4:5:6:7:8",
                "1::2:3:4:5:127.0.0.l",
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1;2::3:4:5:6:7"sv,
                "X:2::3:4:5:6:7"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_4

    BOOST_AUTO_TEST_SUITE(ipv6_address_5)
        // [ *2( h16 ":" ) h16 ] "::" 2( h16 ":" ) ls32
        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2:3::4:5:6:7"sv,
                "1:2::3:4:5:6"sv,
                "1::2:3:4:5"sv,
                "::1:2:3:4"sv,
                "1:2:3::4:5:127.0.0.1"sv,
                "1:2::3:4:127.0.0.1"sv,
                "1::2:3:127.0.0.1"sv,
                "::1:2:127.0.0.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1:2:3::4:5:6:7:8",
                "1:2::3:4:5:6:7:8",
                "1::2:3:4:5:6:7:8",
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1:2:3;:4:5:6:7"sv,
                "1:2:3;:4:X:6:7"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_5

    BOOST_AUTO_TEST_SUITE(ipv6_address_6)
        // [ *3( h16 ":" ) h16 ] "::" h16 ":" ls32
        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2:3:4::5:6:7"sv,
                "1:2:3::4:5:6"sv,
                "1:2::3:4:5"sv,
                "1::2:3:4"sv,
                "::1:2:3"sv,
                "1:2:3:4::5:127.0.0.1"sv,
                "1:2:3::4:127.0.0.1"sv,
                "1:2::3:127.0.0.1"sv,
                "1::2:127.0.0.1"sv,
                "::1:127.0.0.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1:2:3:4::5:6:7:8",
                "1:2:3::4:5:6:7:8",
                "1:2::3:4:5:6:7:8",
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1:2:3:4;:5:6:7"sv,
                "1:2:3;:4:X:6"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_6

    BOOST_AUTO_TEST_SUITE(ipv6_address_7)

        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2:3:4:5::5:6"sv,
                "1:2:3:4::5:6"sv,
                "1:2:3::4:5"sv,
                "1:2::3:4"sv,
                "1::2:3"sv,
                "::1:2"sv,
                "1:2:3:4:5::127.0.0.1"sv,
                "1:2:3:4::127.0.0.1"sv,
                "1:2:3::127.0.0.1"sv,
                "1:2::127.0.0.1"sv,
                "1::127.0.0.1"sv,
                "::127.0.0.1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1:2:3:4:5::6:7:8",
                "1:2:3:4::5:6:7:8",
                "1:2:3::4:5:6:7:8",
                "1:2::3:4:5:6:7:8",
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1:2:3:4:5;:5:6"sv,
                "1:2:3;:4:X:6"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_7

    BOOST_AUTO_TEST_SUITE(ipv6_address_8)

        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2:3:4:5:6::7"sv,
                "1:2:3:4:5::6"sv,
                "1:2:3:4::5"sv,
                "1:2:3::4"sv,
                "1:2::3"sv,
                "1::2"sv,
                "::1"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(partial_)
        {
            partial(validator::ipv6_address, {
                "1:2:3:4:5:6::7:8",
                "1:2:3:4:5::6:7:8",
                "1:2:3:4::5:6:7:8",
                "1:2:3::4:5:6:7:8",
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ipv6_address, {
                "1:2:3:4:5:6;:5"sv,
                "1:2:3:4:5;:5"sv,
                "1:2:3;:4:X"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_8

    BOOST_AUTO_TEST_SUITE(ipv6_address_9)

        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ipv6_address, {
                "1:2:3:4:5:6:7::"sv,
                "1:2:3:4:5:6::"sv,
                "1:2:3:4:5::"sv,
                "1:2:3:4::"sv,
                "1:2:3::"sv,
                "1:2::"sv,
                "1::"sv,
                "::"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ipv6_address_9

    BOOST_AUTO_TEST_SUITE(ip_literal_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            success(validator::ip_literal, {
                "[::]"sv,
                "[1:2:3:4:5:6:7:8]"sv,
                "[v9.foo:bar]"sv,
            });
        }

        BOOST_AUTO_TEST_CASE(fail_)
        {
            fail(validator::ip_literal, {
                "[::"sv,
                "1:2:3:4:5:6:7:8]"sv,
                "[192.168.1.1]"sv,
            });
        }

    BOOST_AUTO_TEST_SUITE_END() // ip_literal_

BOOST_AUTO_TEST_SUITE_END() // non_terminal_

BOOST_AUTO_TEST_SUITE_END() // validator_

} // namespace testing
