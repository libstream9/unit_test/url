#include "src/detail/validator.hpp"

#include "../utility.hpp"

#include <string_view>
#include <vector>

#include <boost/format.hpp>
#include <boost/test/unit_test.hpp>

namespace validator = stream9::urls::validator;

template<typename Parser>
void
test_success(Parser parser,
    std::vector<std::string_view> const& strings,
    validator::error_handler_t on_error = validator::null_error_handler)
{
    for (auto const& s: strings) {
        BOOST_CHECK_MESSAGE(
            parser(s, on_error),
            boost::format("incorrectly failed: %s") % s
        );
    }
}

template<typename Parser>
void
test_fail(Parser parser,
    std::vector<std::string_view> const& strings,
    validator::error_handler_t on_error = validator::null_error_handler)
{
    for (auto const& s: strings) {
        BOOST_CHECK_MESSAGE(
            !parser(s, on_error),
            boost::format("incorrectly succeeded: %s") % s
        );
    }
}

namespace testing {

BOOST_AUTO_TEST_SUITE(validator_)

BOOST_AUTO_TEST_SUITE(uri_component_)

BOOST_AUTO_TEST_SUITE(scheme_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::scheme, {
            "http", "ftp", "https",
            "HTTP", "FTP", "HTTPS",
            "foo+bar", "foo-1.0",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::scheme, {
            "0ftp", "abc|", "bar:"
        });
    }

BOOST_AUTO_TEST_SUITE_END() // scheme_

BOOST_AUTO_TEST_SUITE(user_info_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::user_info, {
            "foo:bar", "foo%2Fbar"
            "foo!bar", "ABC=DEF",
            ""
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::user_info, {
            "foo/bar",
        });
    }

BOOST_AUTO_TEST_SUITE_END() // user_info_

BOOST_AUTO_TEST_SUITE(port_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::port, {
            "12345",
            "",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::port, {
            "foo",
            "123d",
        });
    }

BOOST_AUTO_TEST_SUITE_END() // port_

BOOST_AUTO_TEST_SUITE(path_abempty_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::path_abempty, {
            "/foo", "/bar/xyzzy", "/", "//", ""
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::path_abempty, {
            "foo", "bar?",
        });
    }

BOOST_AUTO_TEST_SUITE_END() // path_abempty_

BOOST_AUTO_TEST_SUITE(path_absolute_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::path_absolute, {
            "/foo", "/", "/foo/bar/xyzzy/"
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::path_absolute, {
            "", "foo", "//", "/foo|"
        });
    }

BOOST_AUTO_TEST_SUITE_END() // path_absolute_

BOOST_AUTO_TEST_SUITE(path_rootless_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::path_rootless, {
            "foo", "foo/bar/", "foo/bar/xyzzy/", "foo///",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::path_rootless, {
            "/", "", "/foo/bar", "foo|"
        });
    }

BOOST_AUTO_TEST_SUITE_END() // path_rootless_

BOOST_AUTO_TEST_SUITE(path_empty_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::path_empty, {
            "",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::path_empty, {
            "foo", "/foo",
        });
    }

BOOST_AUTO_TEST_SUITE_END() // path_empty_

BOOST_AUTO_TEST_SUITE(query_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::query, {
            "abc",
            "a=b&c=d&d=f",
            //"a=/bcd&b=g?",
            "a=/bcd&b=g",
            "",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::query, {
            "abc|", "b=[cde]", "|def|", "[abd]"
        });
    }

BOOST_AUTO_TEST_SUITE_END() // query_

BOOST_AUTO_TEST_SUITE(fragment_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::fragment, {
            "", "abc",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::fragment, {
            "[abd]"
        });
    }

BOOST_AUTO_TEST_SUITE_END() // fragment_

BOOST_AUTO_TEST_SUITE(host_)

    BOOST_AUTO_TEST_CASE(success)
    {
        test_success(validator::host, {
            "[1::2:3:4:5:6:7]",
            "192.168.1.1",
            "host.domain.com",
        });
    }

    BOOST_AUTO_TEST_CASE(fail)
    {
        test_fail(validator::host, {
            "host.domain.com/path"
        });
    }

BOOST_AUTO_TEST_SUITE_END() // host_

BOOST_AUTO_TEST_SUITE_END() // uri_component_

BOOST_AUTO_TEST_SUITE_END() // validator_

} // namespace testing
