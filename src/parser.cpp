#include <stream9/urls/parser.hpp>

#include <iostream>

#include <boost/test/unit_test.hpp>
#include <boost/timer/timer.hpp>

#include <stream9/errors.hpp>

namespace uri {

using namespace std::literals;
namespace urls { using namespace stream9::urls; }

BOOST_AUTO_TEST_SUITE(parser)

void test(std::string_view const input, urls::components const& expected)
{
    auto result = urls::parse(input);

    BOOST_CHECK_EQUAL(result.scheme, expected.scheme);
    BOOST_CHECK_EQUAL(result.user_info, expected.user_info);
    BOOST_CHECK_EQUAL(result.host, expected.host);
    BOOST_CHECK_EQUAL(result.port, expected.port);
    BOOST_CHECK_EQUAL(result.path, expected.path);
    BOOST_CHECK_EQUAL(result.query, expected.query);
    BOOST_CHECK_EQUAL(result.fragment, expected.fragment);
}

void test_error(std::string_view const input)
{
    BOOST_CHECK_THROW(urls::parse(input), stream9::error);
}

BOOST_AUTO_TEST_CASE(complete)
{
    auto const input =
        "http://foo@www.google.com:8080/path?foo=bar#segment"sv;
    urls::components const expected {
        "http",
        "foo",
        "www.google.com",
        "8080",
        "/path",
        "foo=bar",
        "segment"
    };

    test(input, expected);
}

BOOST_AUTO_TEST_CASE(no_authority)
{
    auto const input = "about:blank"sv;
    urls::components const expected {
        "about",
        "",
        "",
        "",
        "blank",
        "",
        ""
    };

    test(input, expected);
}

BOOST_AUTO_TEST_CASE(invalid_but_still_parsable_1)
{
    auto const input = "scheme:path:@path#/bar"sv;
    urls::components const expected {
        "scheme",
        "",
        "",
        "",
        "path:@path",
        "",
        "/bar"
    };

    test(input, expected);
}

BOOST_AUTO_TEST_CASE(invalid_but_still_parsable_2)
{
    auto const input = "http:///www.google.com/index.html"sv;
    urls::components const expected {
        "http",
        "",
        "",
        "",
        "/www.google.com/index.html",
        "",
        ""
    };

    test(input, expected);
}

BOOST_AUTO_TEST_CASE(invalid_but_still_parsable_3)
{
    auto const input = ":"sv;
    urls::components const expected {
        "",
        "",
        "",
        "",
        "",
        "",
        ""
    };

    test(input, expected);
}

BOOST_AUTO_TEST_CASE(parse_error_1)
{
    test_error("aaa/b");
}

BOOST_AUTO_TEST_CASE(parse_error_2)
{
    test_error("");
}

BOOST_AUTO_TEST_CASE(parse_error_3)
{
    test_error("http");
}

BOOST_AUTO_TEST_SUITE_END() // parser

} // namespace uri
