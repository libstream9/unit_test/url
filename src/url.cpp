#include <stream9/url.hpp>

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <cstring>

#include <boost/test/unit_test.hpp>

namespace testing {

using stream9::url;
using stream9::string;
using stream9::string_view;

BOOST_AUTO_TEST_SUITE(url_)

BOOST_AUTO_TEST_SUITE(constructor_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        url u1 { "http://example.com" };
        url u2 { u1 };

        BOOST_TEST(u1 == u2);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        url u1 { "http://example.com" };
        url u2 { std::move(u1) };

        BOOST_TEST(u2 == "http://example.com");
    }

    BOOST_AUTO_TEST_CASE(string_copy_)
    {
        string s = "http://www.sample.com";

        url u { s };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(string_move_)
    {
        string s = "http://www.sample.com";

        url u { std::string(s) };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        string_view s = "http://www.sample.com";

        url u { s };

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        auto s1 = "http://www.sample.com";
        url u1 { s1 };
        BOOST_TEST(u1 == s1);

        auto s2 = "http://www.google.com";
        u1 = s2;
        BOOST_TEST(u1 == s2);
    }

    BOOST_AUTO_TEST_CASE(string_view_convertible_)
    {
        struct foo {
            operator string_view() const { return "http://www.foo.com"; }
        };

        foo f;
        url u { f };

        BOOST_TEST(u == "http://www.foo.com");
    }

BOOST_AUTO_TEST_SUITE_END() // constructor_

BOOST_AUTO_TEST_SUITE(assignment_)

    BOOST_AUTO_TEST_CASE(copy_)
    {
        url u1 { "http://example.com" };
        url u2 { "http://sample.com" };

        u2 = u1;

        BOOST_TEST(u2 == u1);
    }

    BOOST_AUTO_TEST_CASE(move_)
    {
        url u1 { "http://example.com" };
        url u2 { "http://sample.com" };

        u2 = std::move(u1);

        BOOST_TEST(u2 == "http://example.com");
    }

    BOOST_AUTO_TEST_CASE(string_copy_)
    {
        string s = "http://www.sample.com";
        url u { "http://example.com" };

        u = s;

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(string_move_)
    {
        string s = "http://www.sample.com";
        url u { "http://example.com" };

        u = std::move(s);

        BOOST_TEST(u == "http://www.sample.com");
    }

    BOOST_AUTO_TEST_CASE(string_view_)
    {
        string_view s = "http://www.sample.com";
        url u { "http://example.com" };

        u = s;

        BOOST_TEST(u == s);
    }

    BOOST_AUTO_TEST_CASE(pointer_)
    {
        auto s = "http://www.sample.com";
        url u { "http://example.com" };

        u = s;

        BOOST_TEST(u == s);
    }

BOOST_AUTO_TEST_SUITE_END() // assignment_

BOOST_AUTO_TEST_SUITE(conversion_)

    BOOST_AUTO_TEST_CASE(to_string_)
    {
        url u { "http://example.com" };

        string s { u };

        BOOST_TEST(s == u);
    }

    BOOST_AUTO_TEST_CASE(to_string_view_)
    {
        url u { "http://example.com" };

        string_view s = u;

        BOOST_TEST(s == u);
    }

BOOST_AUTO_TEST_SUITE_END() // conversion_

BOOST_AUTO_TEST_SUITE_END() // url_

} // namespace testing
